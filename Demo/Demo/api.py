﻿from flask import Response, request, render_template, redirect, url_for, flash, json, Blueprint, session
#from flask.ext.login import login_user, login_required, logout_user
#from flask.ext.triangle import Triangle
import pprint
import time
from Demo import app
#import pypyodbc
import hashlib
import base64
import pdb
import database as dbs
import pymysql.cursors

api_blueprint = Blueprint('api', __name__)

CON_STR="Connection"
STATIC_FOLDER = app.static_folder
database = dbs.Database(CON_STR, STATIC_FOLDER)

@api_blueprint.route('getImageUrl', methods=['GET'])
def getImageUrl():
    data = dict()
    data['images'] = [
        '/static/images/templates/scratch.jpg',
        '/static/images/templates/R1.jpg','/static/images/templates/R2.jpg','/static/images/templates/R3.jpg',
        '/static/images/templates/R4.jpg','/static/images/templates/R5.jpg','/static/images/templates/R6.jpg',
        '/static/images/templates/R7.jpg','/static/images/templates/R8.jpg','/static/images/templates/R9.jpg'
        ]
    data['elements'] = ['/static/images/filterImages/filter1.png','/static/images/filterImages/filter2.png','/static/images/filterImages/filter3.png']
    #data['test']=database.GetImageUrl()
    #cursor = mysql.connect().cursor()
    #cursor.execute("SELECT * from Testing")
    #data['test'] = cursor.fetchall()
    return Response(json.dumps(data, sort_keys=True, indent=4, separators=(',', ': ')), mimetype='text/json'), 200

@api_blueprint.route('getCategoryNames', methods=['GET'])
def getCategoryNames():
    data=['Thanksgiving','Birthday','Wedding','Bachelorette','Christmas','Newyear','BridalShower','Celebration','Business','Baby Shower','College','Kids','Generic']
    #data['url']=database.GetImageUrl()
    return Response(json.dumps(data, sort_keys=True, indent=4, separators=(',', ': ')), mimetype='text/json'), 200